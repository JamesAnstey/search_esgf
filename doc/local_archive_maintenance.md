## Local archive maintenance

`check_archive.py` is a script in `bin/` that can perform maintenance checks on a local archive.
This helps to ensure the dir structure, dir names, permissions, etc are as expected.
Assuming an archive is shared by many users, this housekeeping utility is meant to help everyone be good citizens by flagging any errors that arise.
Running this script doesn't alter the local archive in any way; it doesn't *fix* any errors. 
Fixing problems is the user's responsibility, but the diagnostic report produced by `check_archive.py` is meant to help with that.
Daily reports are [available here](https://goc-dx.science.gc.ca/~rja001/ESGF_downloads/maintenance_reports/).

It has a command-line interface, with options visible using the `-h` / `--help` option:
```
./check_archive.py -h
```
For example, to check all the way down to the lowest level ("version", where the netcdf files are located) use:
```
./check_archive.py -d version
```
An abridged version of the results appears on the stdout, and a full report goes into a dir that will be indicated on the standard out, for example `maintenance/reports/10Dec2022`. 
The report is a txt file listing problems, and user-specific dirs contain files that list the dirs with specific problems owned by that user.
Example of what's in a report dir (if the `tree` command were invoked in the dir):
```
├── report.txt
├── usr001
│   ├── empty_dirs.txt
│   └── empty_files.txt
├── usr002
    └── wrong_dir_permissions.txt
```
In this report, user `usr001` owns some empty dirs, listed in `empty_dirs.txt`, and also owns some dirs containing at least one empty file, listd in `empty_files.txt`.
User `usr002` owns some dirs that have their permissions set incorrectly for the shared archive.
(A likely cause of that error is not allowing group write permission, which is needed so that all users can download to the shared archive.)

`check_archive.py` does not actually *fix* anything.
Users need to fix their own errors, since they are the owners of the dirs with errors.
The text files in the report dir are meant to help.
From the example above, `usr002` could do
```
chmod g+w `cat wrong_dir_permissions.txt`
```
to enable group write permission on the offending dirs.

Just for information, the default local archive location can be displayed by:
```
../bin/check_archive.py --where
```
If the local archive is not at the default path, the `-a` argument can be used to specify it.

For reference the names of valid CV (controlled vocabularly) parameters can be displayed using:
```
../bin/check_archive.py --params
```
which shows something like:
```
Parameter names by directory tree level:
  level  depth (parameter name)
  -----  ----------------------
      1  mip_era
      2    activity_drs
      3      institution_id
      4        source_id
      5          experiment_id
      6            member_id
      7              table_id
      8                variable_id
      9                  grid_label
     10                    version
```
The depth can also be specified as the level integer (above) using the `-l` input argument.
For example, `-d version` is equivalent to `-l 10`.

The check can also be limited to certain parameter values using the `-k` / `--keep` argument. Example:
```
./check_archive.py -d version -k activity_drs=CDRMIP,C4MIP
```
The values must be given as `parameter=value1,value2,...` with no spaces.
To specify values of more than one parameter, separate the parameters by a space and enclose the whole string in quotes, for example:
```
./check_archive.py -d version -k "activity_drs=CDRMIP,C4MIP variable_id=thetao"
```

It's useful to check the archive's integrity regularly.
Example of running it as a cron job (use `crontab -e` to edit cron jobs):
```
0 0 * * * cd /home/rja001/code/search_esgf/bin/ && ./check_archive.py --depth version
```
runs it at midnight every day (https://www.adminschoice.com/crontab-quick-reference).

Daily reports by `check_archive.py` are [available here](https://goc-dx.science.gc.ca/~rja001/ESGF_downloads/maintenance_reports/).

At the end of the report there is also a summary of the usage by each owner (not errors, just an overall summary of how much data is in the archive and who put it there).


## Linking the local archive to point at CCCma output already on sitestore

CCCma output shouldn't be downloaded since it's already on sitestore.
Links can be placed in the local archive at the `institution_id` level for convenience, so that CCCma data is readily accessible from the local archive path.
There is a script in `bin/` to do this automatically, which can be run by:
```
python link_CCCma_output.py
```
For all the MIPs in the target dir, the script checks if a link is already in the local archive, and offers to create one if it's not.
