#!/usr/bin/env python
'''
For maintenance on a shared local archive of downloaded ESGF data.

Goes through the local archive to look for errors, including:
    - incorrect dir or file permissions
    - empty dirs
    - dir names not in the CV (controlled vocabulary)
    The depth to which the check descends is controlled by -d/--depth, e.g.
        --depth version
    descends to the "version" dir at the very bottom of the dir structure,
    i.e.  the max depth in the CMIP6 DRS (data reference syntax), which is
    where the netcdf files reside.

This only checks & reports on errors, it doesn't fix them. 
It doesn't alter the dir tree in any way.
'''
###############################################################################
import os
import sys
if sys.version_info.major == 3:
    from importlib import reload
import time
import json
if sys.version_info.major == 2:
    input = raw_input
import pathlib
import datetime
import subprocess
import argparse

import esgfsearch as es
reload(es)
###############################################################################

# Get local archive path.
local_archive = es.local_archive_location()

work_dir = '../maintenance/'

# Command line arguments
parser = argparse.ArgumentParser()

# These arguments simply provide the user with info and then exit:
parser.add_argument('-a', '--archive', type=str, default=local_archive, help='Set path of local archive')
parser.add_argument('--where', default=False, action='store_true', help='Show local archive location and exit')

# These arguments pertain to checking of the local archive dir & files:
parser.add_argument('-d', '--depth', type=str, default='experiment_id', help='Depth in directory to descend when checking (given as parameter name)')
parser.add_argument('-l', '--level', type=int, default=None, help='Level in directory to descend when checking (given as as integer; overrides -d/--depth option)')
parser.add_argument('-n', '--name', type=str, default='', help='name to append to report dir name (following the date)')
parser.add_argument('--params', default=False, action='store_true', help='Display parameter names for levels in directory tree and exit')
parser.add_argument('--followlinks', default=False, action='store_true', help='Follow symbolic links when checking directory tree')
parser.add_argument('-k', '--keep', type=str, default='', help='Restrict check to specified parameter values')

# Miscellaneous:
parser.add_argument('--save_CV_info', default=False, action='store_true', help='Save info about the CV in a json file and exit')

args = parser.parse_args()

local_archive = args.archive

write_report = True
write_owner_reports = True

###############################################################################

if not os.path.exists(local_archive):
    raise Exception('Could not find local archive: ' + local_archive)
    sys.exit()

if args.where:
    print('Local archive:  ' + local_archive)
    sys.exit()

params = es.OUTPUT_PATH_TEMPLATE[1:]
# indicate_value_kept = '--'
indicate_value_kept = '  *'
def show_param_names(params, as_msg=False, keep={}):
    indent = '  '
    # connector = '└── '  # like the 'tree' utility, but this character doesn't display properly in web browser
    # connector = '> '
    connector = '  '
    l = []
    l.append('level  depth (parameter name)')
    l.append('-----  ----------------------')
    fmt_level = '%5s'
    max_lines = 0
    for k,p in enumerate(params):
        sep = indent
        # sep = '--'
        if k == 0: s = p
        else: s = sep*(k-1) + connector + p
        l.append('{level}  {depth}'.format(level=fmt_level % str(k+1), depth=s))
        max_lines += 1
        if p in keep:
            for v in keep[p]:
                s = indent*(k-1) + ' '*len(connector) + indicate_value_kept + ' ' + v
                l.append('{level}  {depth}'.format(level=fmt_level % ' ', depth=s))
                max_lines += 1
    l = [indent + s for s in l]
    if as_msg:
        msg(l, max_lines_stdout=max_lines+2)
    else:
        print('\n'.join(l))

if args.params:
    print('Parameter names by directory tree level:')
    show_param_names(params)
    sys.exit()

if args.depth not in params:
    print('Invalid depth parameter: ' + args.depth)
    print('Valid parameter names:')
    show_param_names(params)
    sys.exit()

keep = {}
if len(args.keep) > 0:
    print(args.keep)
    l = args.keep.split()
    for s in l:
        if s.count('=') != 1: continue
        p,values = s.split('=')
        values = values.split(',')
        keep[p] = values

###############################################################################
# Load the Controlled Vocabulary (CV) info.
path_CV = os.path.join(work_dir, 'cmip6-cmor-tables/Tables')
filename = 'CMIP6_CV.json'
filepath = os.path.join(path_CV, filename)
if not os.path.exists(filepath):
    raise Exception('CMOR tables not found. \n\nGet CMOR tables using:\n  git clone https://github.com/PCMDI/cmip6-cmor-tables\n')
# Handy links to the CMOR tables:
# https://wcrp-cmip.github.io/CMIP6_CVs    
# https://github.com/WCRP-CMIP/CMIP6_CVs
# https://github.com/PCMDI/cmip6-cmor-tables 
# https://github.com/PCMDI/cmip6-cmor-tables/blob/master/Tables/CMIP6_CV.json
param_name = {p : p for p in params}
param_name.update({'activity_drs' : 'activity_id'})
param_not_in_CV = ['member_id', 'version']
CV = {}
with open(filepath, 'r') as f:
    d = json.load(f)
    for p in param_name:
        if p in param_not_in_CV:
            continue
        elif p == 'variable_id':
            # variable names are loaded separately, below
            CV[p] = set()
        else:
            CV[p] = set( d['CV'][param_name[p]] )
# The above gets all required CV values except variable names.
for table_id in CV['table_id']:
    filename = 'CMIP6_{table}.json'.format(table=table_id)
    filepath = os.path.join(path_CV, filename)
    with open(filepath, 'r') as f:
        d = json.load(f)
        CV['variable_id'].update(d['variable_entry']) # add keys of d['variable_entry'] dict to set of variable names
    del d
for p in CV:
    assert p in params, 'Unknown parameter in CV dict: ' + p

if args.save_CV_info:
    # Save info about the CV for use elsewhere (it's used by link_CCCma_output.py), then exit.
    filename = 'CV_info.json'
    filepath = os.path.join(work_dir, filename)
    CV = {p : sorted(list(v), key=str.lower) for p,v in CV.items()} # python set() variables can't be saved in json
    with open(filepath, 'w') as f:
        json.dump(CV, f, indent=2, sort_keys=True)
        print('Wrote ' + filepath)
    sys.exit()

# Specify any dirs in the archive that aren't CV terms but we've purposely included 
# (i.e., these are not errors in the dir structure!). This just simplifies the reporting.
ignore = set()
ignore.add( os.path.join(local_archive, 'intake-esm-datastore') )
ignore.add( os.path.join(local_archive, 'CMIP3_OTHER_DOWNLOADS') )
ignore.add( os.path.join(local_archive, 'CMIP5_OTHER_DOWNLOADS') )
ignore.add( os.path.join(local_archive, 't') )  # probably someone's typo...

# Specify valid file extensions for netcdf files
valid_file_ext = list(es.VALID_FILE_EXT)

###############################################################################
if write_report:
    # Report dir / logfile setup
    log_dir = os.path.join(work_dir, 'reports')
    def reportname(date, edition=None):
        # s = 'report_{date}'.format(date=date)
        s = '{date}'.format(date=date)
        if len(args.name) > 0:
            s += '_{}'.format(args.name)
        if edition:
            s += '_{edition}'.format(edition='%.3i' % edition)
        return s
    date = datetime.datetime.utcnow().strftime('%d%b%Y')
    use_editions = False
    if use_editions:
        edition = 1
        report = reportname(date, edition)
        ld = os.listdir(log_dir)
        while report in ld:
            edition += 1
            report = reportname(date, edition)
    else:
        report = reportname(date)
    log_dir = os.path.join(log_dir, report)
    if not os.path.exists(log_dir): os.makedirs(log_dir)
    logfilepath = os.path.join(log_dir, 'report.txt')
    if os.path.exists(logfilepath): os.remove(logfilepath)
    del date, report

indent = '  '
def msg(lines, stdout=True, max_lines_stdout=6, max_lines_logfile=15):
    if isinstance(lines, str): lines = [lines]
    assert isinstance(lines, list)
    assert all([isinstance(s, str) for s in lines])
    if stdout:
        for s in lines[:max_lines_stdout]:
            print(s)
        remaining_lines = len(lines) - max_lines_stdout
        if remaining_lines > 0:
            print(indent + '+ {n} more lines'.format(n=remaining_lines))
    if write_report:
        if max_lines_logfile is not None:
            remaining_lines = len(lines) - max_lines_logfile
            if remaining_lines > 0:
                lines = lines[:max_lines_logfile]
                lines.append(indent + '+ {n} more lines'.format(n=remaining_lines))
        with open(logfilepath, 'a') as f:
            s = '\n'.join(lines)
            f.write(s + '\n')
###############################################################################
msg('\nDoing maintenance on local archive: ' + local_archive)
msg('Starting time: ' + datetime.datetime.utcnow().strftime('%d %b %Y %H:%M:%S UTC'))

# Specify how far down the dir tree to check.
level = args.level
if level is not None:
    assert isinstance(level, int), 'level must be integer'
    min_level, max_level = 1, len(params)
    assert level >= min_level and level <= max_level, 'level must be between {min} and {max}'.format(min=min_level, max=max_level)
    depth = params[level-1]
else:
    depth = args.depth

# followlinks=True ==> descend into linked dirs, treating them the same as regular dirs.
# Otherwise linked dirs are not checked.
# Assuming the linked dirs are in the CMIP6 final dir, they shouldn't need checking. But can be if needed.
# followlinks = False 
followlinks = args.followlinks

# check_dir_permissions=True ==> check that permissions are as they should be in the local archive.
# For directories, the required permissions are given by es.LOCAL_ARCHIVE_PERMISSIONS.
check_dir_permissions = True

check_file_permissions = True

check_file_sizes = True

# check_usage_stats = True ==> tally up everything each user has added to the local archive.
# This isn't to look for errors, it's to get an overall summary of what each user has downloaded to the archive.
# (Results of course will depend on the depth to which the search descends.)
check_usage_stats = True

check_mip_stats = True

###########################################################################
start_time = time.time()

m = params.index(depth)+1
check = params[:m]
assert len(check) >= 1, 'Must check at least one level of dir tree' 
in_CV = [p for p in check if p not in param_not_in_CV]
not_in_CV = [p for p in check if p in param_not_in_CV]
values = {p : set(CV[p]) for p in in_CV}
msg('\nChecking to a depth of {n} levels in the dir tree:'.format(n=len(check)))
show_param_names(check, as_msg=True, keep=keep)
if len(keep) > 0:
    msg('If only selected parameter values are checked, these are indicated by {}'.format(indicate_value_kept.strip()))
if len(not_in_CV) > 0:
    msg('Parameters not in the CV: ' + ', '.join(not_in_CV))

excluded = []
links = []
empties = []
# Note: variables like 'links', 'empties', etc, are lists instead of sets because I want them to
# be ordered by when the search finds them, i.e. as os.walk() proceeds through the local_archive dir.
path = os.path.normpath(local_archive)
if check_dir_permissions:
    dir_permissions_errors = []
    dir_permissions = {}
    dir_permissions_required = oct(es.LOCAL_ARCHIVE_PERMISSIONS)[-3:]
    link_path = '////' # default value; this string must never start a path found in the os.walk loop
    assert not path.startswith(link_path)
if check_file_permissions or check_file_sizes:
    level_file_check = params.index('version') + 1
    file_permissions_errors = set()
    file_size_errors = set()

testing = False
if testing: 
    print('      * TESTING * ')
    # limit the os.walk loop, just to speed things up when testing the code
    k = 0
    k_max = 13
    k_max = 100

usage_stats = {}
def get_og(path):
    path = pathlib.Path(path)
    owner, group = path.owner(), path.group()
    if (owner,group) not in usage_stats:
        usage_stats[(owner,group)] = {
            'dirs' : {s : 0 for s in check},
            'files' : 0,
            'size' : 0,
        }
    return owner, group
mip_stats = {}

print('')
mip = None
for dirpath, dirnames, filenames in os.walk(path, topdown=True, followlinks=followlinks):

    if testing:
        print(k, dirpath, filenames)
        k += 1
        # if k > k_max: break

    relpath = os.path.relpath(dirpath, path)
    # Determine level we're at in the dir tree, relative to local archive path, and denote this by integer 'level'.
    if relpath == '.':
        level = 0
    else:
        level = len(relpath.split(os.path.sep))

    # print(level, check[level], dirpath, dirnames)

    if level == 2:
        # It can take a while to go through the archive, so let user know where we're at
        print('{time}  Now checking:  {dirpath}'\
            .format(dirpath=dirpath, time=es.elapsed_time_str(time.time() - start_time)))

        mip = os.path.basename(dirpath) # remember what mip we're currently looking at
        mip_stats[mip] = {'files' : 0, 'size' : 0, 'path' : dirpath}

    if check_usage_stats and level > 0:
        p = check[level-1] # name of parameter corresponding to dirpath
        owner, group = get_og(dirpath)
        usage_stats[(owner,group)]['dirs'][p] += 1

    if check_dir_permissions:
        stat = os.stat(dirpath)
        permissions = oct(stat.st_mode)[-3:]
        if os.path.islink(dirpath):
            # In this case os.stat.st_mode will indicate the permissions of the linked dir.
            # They might not be the same as required for the local archive.
            # E.g. in the CMIP6 final dir, permissions are 755 (read-only for group & world).
            link_path = dirpath
        elif dirpath.startswith(link_path):
            # We are in a subdir of a linked path, so the permissions need not be those required
            # for the local archive.
            pass
        else:
            # Check if dir has the correct permissions.
            if permissions != dir_permissions_required:
                dir_permissions_errors.append(dirpath)
                dir_permissions[dirpath] = permissions

    if level == level_file_check:  # We've reached the level where netcdf files are stored
        for filename in filenames:
            s, ext = os.path.splitext(filename)
            if ext not in valid_file_ext:
                # If not a netcdf file, skip it
                continue
            filepath = os.path.join(dirpath, filename)
            stat = os.stat(filepath)
            if check_file_permissions:
                perm = es.user_permissions(stat)
                # Require read permission for all user types
                if not all(['r' in perm[s] for s in perm]):
                    file_permissions_errors.add(dirpath)
            if check_file_sizes:
                if stat.st_size == 0:
                    file_size_errors.add(dirpath)
            if check_usage_stats:
                owner, group = get_og(filepath)
                usage_stats[(owner,group)]['files'] += 1
                usage_stats[(owner,group)]['size'] += stat.st_size
            if check_mip_stats:
                mip_stats[mip]['files'] += 1
                mip_stats[mip]['size'] += stat.st_size

    # Record any links to directories tha are found in the current dir.
    for s in dirnames:
        s = os.path.join(dirpath, s)
        if os.path.islink(s):
            links.append(s)

    if dirnames == [] and filenames == []:
        empties.append(dirpath)

    if level >= len(check): # True if we've descended as far as we should
        for s in list(dirnames):
            # Remove all entries of dirnames so that we don't descend further.
            dirnames.remove(s)
            # (Note, setting dirnames = [] here does not work because it creates
            # a new dirnames variable. The entries have to be removed from dirnames.)
        continue

    p = check[level] # name of parameter to check at this level (e.g. p = 'institution_id')
    if p in values:
        all_dirnames = set(dirnames)
        valid = all_dirnames.intersection(values[p]) # dirnames that are in the CV
        not_in_CV = all_dirnames.difference(values[p]) # dirnames that aren't in the CV
        if p in keep:
            valid = valid.intersection(set(keep[p]))
        exclude = all_dirnames.difference(valid) # dirnames that aren't in the CV, or otherwise excluded
        for s in exclude:
            # Skip excluded dirnames
            dirnames.remove(s)
            if s in not_in_CV:
                # Make a note of the excluded dirnames
                excluded.append(os.path.join(dirpath, s))
    del p



msg('\nTime taken to check to depth of {n} levels in the dir tree (up to {p}): {time}'\
    .format(n=len(check), time=es.elapsed_time_str(time.time() - start_time), p=depth))

owners = set()
def get_owners(l): 
    owners.update([d['owner'] for d in l])

def owner_errors(l):
    errors = {}
    for d in l:
        owner, dirpath = d['owner'], d['dirpath']
        if owner not in errors:
            errors[owner] = []
        errors[owner].append(dirpath)
    return errors

if len(links) > 0:
    msg('\nFound {n} symlinks:'.format(n=len(links)))
    fmt = '%-{m}s'.format(m=max([len(s) for s in links]))
    # msg([indent + '{link} --> {target}'.format(link=fmt % link, target=pathlib.Path(link).resolve()) for link in links])
    paths = [pathlib.Path(link) for link in links]
    l = [{'link':fmt % link, 'target':path.resolve(), 'owner':path.owner()} for link,path in zip(links,paths)]
    msg([indent + '{owner} {link} -> {target}'.format(**d) for d in l])
    if not followlinks:
        msg('Dir structure for symlinks was not checked (to check it, use --followlinks option)')
    get_owners(l)

if len(dir_permissions_errors) > 0:
    msg('\nFound {n} directories with permission errors:'.format(n=len(dir_permissions_errors)))
    paths = [pathlib.Path(dirpath) for dirpath in dir_permissions_errors]
    l = [{'dirpath':dirpath, 'permissions':dir_permissions[dirpath], 'owner':path.owner()} for dirpath,path in zip(dir_permissions_errors,paths)]
    msg([indent + '{owner} {permissions} {dirpath}'.format(**d) for d in l])
    msg('Expected permissions: {permissions}'.format(permissions=dir_permissions_required))
    get_owners(l)
    dir_permissions_errors = owner_errors(l)

if len(empties) > 0:
    msg('\nFound {n} empty directories:'.format(n=len(empties)))
    paths = [pathlib.Path(dirpath) for dirpath in empties]
    l = [{'dirpath':dirpath, 'owner':path.owner()} for dirpath,path in zip(empties, paths)]
    msg([indent + '{owner} {dirpath}'.format(**d) for d in l])
    get_owners(l)
    empties = owner_errors(l)

if len(file_permissions_errors) > 0:
    file_permissions_errors = list(file_permissions_errors)
    msg('\nFound {n} directories whose files have permission errors:'.format(n=len(file_permissions_errors)))
    paths = [pathlib.Path(dirpath) for dirpath in file_permissions_errors]
    l = [{'dirpath':dirpath, 'owner':path.owner()} for dirpath,path in zip(file_permissions_errors, paths)]
    msg([indent + '{owner} {dirpath}'.format(**d) for d in l])
    get_owners(l)
    file_permissions_errors = owner_errors(l)

if len(file_size_errors) > 0:
    file_size_errors = list(file_size_errors)
    msg('\nFound {n} directories containing empty files:'.format(n=len(file_size_errors)))
    paths = [pathlib.Path(dirpath) for dirpath in file_size_errors]
    l = [{'dirpath':dirpath, 'owner':path.owner()} for dirpath,path in zip(file_size_errors, paths)]
    msg([indent + '{owner} {dirpath}'.format(**d) for d in l])
    get_owners(l)
    file_size_errors = owner_errors(l)

excluded = [s for s in excluded if s not in ignore]
if len(excluded) == 0:
    msg('\nNo invalid paths found')
else:
    msg('\nFound {n} paths not in the CV:'.format(n=len(excluded)))
    #msg([indent + s for s in excluded])
    # l = [{'dirpath':dirpath, 'owner':pathlib.Path(dirpath).owner()} for dirpath in excluded]
    paths = [pathlib.Path(dirpath) for dirpath in excluded]
    l = [{'dirpath':dirpath, 'owner':path.owner()} for dirpath,path in zip(excluded, paths)]
    msg([indent + '{owner} {dirpath}'.format(**d) for d in l])
    get_owners(l)
    not_in_CV = owner_errors(l)

def get_owner_real_name(account):
    '''
    Find real name of the account owner.
    Example:
      [rja001@hpcr5-vis1 bin]$ pinky -l rja001
      Login name: rja001                      In real life:  James Anstey
      Directory: /home/rja001                 Shell:  /bin/bash
    '''
    cmd = 'pinky -l {user}'.format(user=account)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    stdout, stderr = proc.communicate()
    return stdout.decode('utf-8').strip().split('\n')[0].rpartition('In real life: ')[-1].strip()

names = {}
if len(owners) > 0:
    for account in sorted(owners):
        names[account] = get_owner_real_name(account)
    msg('\nAccount owners:')
    fmt = '%-{m}s'.format(m=max([len(account) for account in names]))
    l = [{'account':fmt % account, 'name':names[account]} for account in names]
    msg([indent + '{account}  {name}'.format(**d) for d in l])

cases = {
    'empty dirs' : empties,
    'empty files' : file_size_errors,
    'wrong dir permissions' : dir_permissions_errors,
    'wrong file permissions' : file_permissions_errors,
    'paths not in CV' : not_in_CV,
}
for case in cases:
    d = cases[case]
    if len(d) == 0: continue
    msg('\nNumber of {}, by owner:'.format(case))
    for owner in d:
        n = len(d[owner])
        msg('  {}: {}'.format(owner,n))
if write_owner_reports:
    if any([ len(cases[s]) > 0 for s in cases ]):
        print('\nWriting owner-specific error lists:')
        for case in cases:
            d = cases[case]
            for owner in d:
                path = os.path.join(log_dir, owner)
                if not os.path.exists(path):
                    os.makedirs(path)
                filename = case.replace(' ', '_') + '.txt'
                filepath = os.path.join(path, filename)
                w = '\n'.join(d[owner]) + '\n'
                with open(filepath, 'w') as f:
                    f.write(w)
                    print('  ' + os.path.abspath(filepath))
msg('')

if check_usage_stats:
    owners = sorted(set([k[0] for k in usage_stats.keys()]))
    groups = sorted(set([k[1] for k in usage_stats.keys()]))
    indent = '  '
    show_dirs = False
    total = {'files' : 0, 'size' : 0}
    total_group = {group : {'files' : 0, 'size' : 0} for group in groups}
    lw = []
    s = '   Overall usage summary by user   '
    line = '-'*len(s)
    lw.append('\n{}\n{}\n{}\n'.format(line, s, line))
    lw.append('Owners ({}):'.format(len(owners)))
    for owner in owners:
        lw.append(indent + owner)
    lw.append('Groups ({}):'.format(len(groups)))
    for group in groups:
        lw.append(indent + group)
    lw.append('\nUsage by owner & group:')
    for owner in owners:
        m = 1  # indent level
        if owner not in names:
            names[owner] = get_owner_real_name(owner)
        lw += ['\n' + indent*m + '{} ({})'.format(owner, names[owner])]
        for group in groups:
            k = (owner,group)
            if k not in usage_stats:
                continue
            d = usage_stats[k]
            m = 2
            lw.append(indent*m + '{}:'.format(group))
            # l.append(indent*m + group)
            m += 1
            lw.append(indent*m + 'total number of files: {}'.format(d['files']))
            lw.append(indent*m + 'total size of files: {}'.format(es.file_size_str(d['size'])))
            if show_dirs:
                lw.append(indent*m + 'number of dirs at each level:')
                m += 1
                for p in check:
                    lw.append(indent*m + '{}: {}'.format(p, d['dirs'][p]))
            for s in 'files', 'size':
                total[s] += d[s]
                total_group[group][s] += d[s]
    
    lw += ['\nTotal usage, by group:']
    for group in groups:
        m = 1
        lw.append(indent*m + group)
        m += 1
        d = total_group[group]
        lw.append(indent*m + 'total number of files: {}'.format(d['files']))
        lw.append(indent*m + 'total size of files: {}'.format(es.file_size_str(d['size'])))

    lw += ['\nTotal usage (all owners):']
    m = 1
    d = total
    lw.append(indent*m + 'total number of files: {}'.format(d['files']))
    lw.append(indent*m + 'total size of files: {}'.format(es.file_size_str(d['size'])))

    w = '\n'.join(lw) + '\n'
    msg(w, max_lines_logfile=len(lw)+5)

if check_mip_stats:
    indent = '  '
    total = {'files' : 0, 'size' : 0}
    mips = set(mip_stats.keys())
    lw = []
    s = '   Overall usage summary by MIP   '
    line = '-'*len(s)
    lw.append('\n{}\n{}\n{}\n'.format(line, s, line))
    lw.append('MIPs ({}):'.format(len(mips)))
    for mip in mips:
        lw.append(indent + mip)
    lw.append('\nUsage by MIP:')
    for mip in mips:
        d = mip_stats[mip]
        m = 1  # indent level
        lw += ['\n' + indent*m + '{}'.format(mip)]
        m += 1
        lw.append(indent*m + d['path'])
        lw.append(indent*m + 'total number of files: {}'.format(d['files']))
        lw.append(indent*m + 'total size of files: {}'.format(es.file_size_str(d['size'])))
        for s in 'files', 'size':
            total[s] += d[s]
    # Total of everything searched
    lw += ['\nTotal usage (all MIPs):']
    m = 1
    lw.append(indent*m + 'total number of files: {}'.format(total['files']))
    lw.append(indent*m + 'total size of files: {}'.format(es.file_size_str(total['size'])))
    
    w = '\n'.join(lw) + '\n'
    msg(w, max_lines_logfile=len(lw)+5)

if write_report:
    print('Full report: {}\n'.format(os.path.abspath(logfilepath)))
