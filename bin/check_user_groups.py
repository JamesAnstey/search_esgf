#!/usr/bin/env pythong
'''
Script to easily check who is in a group (eccc_crd) and if specific users are in the group.
'''


import os
import pwd
import grp
import subprocess


check_users = []
check_users.append('rja001')
check_users.append('rsa001')


use_group = 'eccc_crd'


def get_owner_real_name(account):
    '''
    Find real name of the account owner.
    Example:
      [rja001@hpcr5-vis1 bin]$ pinky -l rja001
      Login name: rja001                      In real life:  James Anstey
      Directory: /home/rja001                 Shell:  /bin/bash
    '''
    cmd = 'pinky -l {user}'.format(user=account)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    stdout, stderr = proc.communicate()
    return stdout.decode('utf-8').strip().split('\n')[0].rpartition('In real life: ')[-1].strip()

group_info = grp.getgrnam(use_group)

real_name = {}
for username in set(group_info.gr_mem + check_users):
    real_name[username] = get_owner_real_name(username)

print('group name: ' + group_info.gr_name)
print('group members: ')

# sort alphabetically by first name
l = [ (name, username) for username, name in  real_name.items()]
l.sort()
sorted_names = [t[-1] for t in l]

for username in sorted_names:
    print('  {}  {}'.format('%-8s' % username, '%s' % real_name[username]))

print('\nChecking if users in the group:')
for username in check_users:
    in_group = 'no'
    if username in group_info.gr_mem: in_group = 'yes'
    print('  {}  {}  {}'.format('%-4s' % in_group, '%-8s' % username, '%s' % real_name[username]))
        
