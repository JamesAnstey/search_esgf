#!/usr/bin/env python
'''
Put links to CCCma data into the local archive.
This code isn't general - it assumes the CMIP6 path structure, and only inserts
links at the 'institute_id' level of the dir tree.
'''
import os
import sys
if sys.version_info.major == 3:
    from importlib import reload
import json
if sys.version_info.major == 2:
    input = raw_input
import pathlib

import esgfsearch as es
reload(es)

# Get local archive path.
local_archive = es.local_archive_location()

# Specify path to which the local archive will be linked (e.g. the CCCma CMIP6 "final" dir on sitestore).
linked_archive = '/space/hall5/sitestore/eccc/crd/ccrn/model_output/CMIP6/final'

# Load the Controlled Vocabulary (CV) info.
work_dir = '../maintenance/'
filename = 'CV_info.json'
filepath = os.path.join(work_dir, filename)
with open(filepath, 'r') as f:
    CV = json.load(f)
    print('Loaded ' + filepath)
CV = {p : set(v) for p,v in CV.items()}

mip_era = 'CMIP6'
institution_id = 'CCCma'
mips = os.listdir(os.path.join(linked_archive, mip_era))

print('\nChecking for links from local archive:')
print('  ' + local_archive)
print('to linked archive:')
print('  ' + linked_archive)

summary = {}  
create_link = {}
for mip in sorted(mips):
    print('\nChecking {institute} links for MIP: {mip}'.format(institute=institution_id, mip=mip))
    assert mip in CV['activity_drs'], 'MIP not in CV: ' + mip
    relpath = os.path.join(mip_era, mip, institution_id)
    target = os.path.join(linked_archive, relpath) # link will point at this
    assert os.path.exists(target), 'Target not found: ' + target
    link = os.path.join(local_archive, relpath)
    if os.path.exists(link):
        if os.path.islink(link):
            # print('Link already exists: {link}'.format(link=link))
            existing_link_target = pathlib.Path(link).resolve()
            desired_link_target = pathlib.Path(target).resolve()
            if existing_link_target == desired_link_target:
                summary[mip] = 'ok'
            else:
                print('Existing link points at wrong target:')
                print('  Desired target: {}'.format(desired_link_target))
                print('  Existing target: {}'.format(existing_link_target))
                summary[mip] = 'incorrect link target'
        else:
            print('Path already exists but is not a link: {link}'.format(link=link))
            summary[mip] = 'path exists but is not a link'
    else:
        print('Need to create link:\n  {link} --> {target}'.format(link=link, target=target))
        summary[mip] = 'need to create link'
        create_link[mip] = {'target' : target, 'link' : link}
        
print('\nSummary:')
fmt = '%-{}s'.format(max([len(mip) for mip in mips]))
for mip in sorted(mips):
    print('  {mip} {status}'.format(mip=fmt % mip, status=summary[mip]))

if len(create_link) > 0:
    response = input('\nCreate links? y for yes, anything else for no ')
    if response.lower() in ['y']:
        for mip in sorted(create_link):
            target, link = create_link[mip]['target'], create_link[mip]['link']
            # check if MIP dir exists
            mip_dir = os.path.dirname(link)
            if os.path.exists(mip_dir):
                assert os.path.isdir(mip_dir), mip_dir + ' is not a directory! what gives?'
            else:
                print('Creating MIP dir: ' + mip_dir)
                os.makedirs(mip_dir)
                os.chmod(mip_dir, 0o775)
            # create link to CCCma model output dir (the "final" dir)
            print('Creating link:\n {link} --> {target}'.format(link=link, target=target))
            os.symlink(target, link)
else:
    print('\nAll {} MIPs for institution_id={} in'.format(len(mips), institution_id))
    print('  ' + linked_archive)
    print('are already linked from')
    print('  ' + local_archive)
