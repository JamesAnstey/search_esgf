#!/usr/bin/env python
'''
Set up working directory for a new search.

This script creates a new working directory (a subdirectory of the "searches"
directory). It then copies bin/search.py into that directory, and edits the
paths in the new working copy of search.py. This new copy of search.py in the
working directory provides a template that the user can modify as needed to
carry out searches.
'''

import argparse
import re
import os
import subprocess
import sys

recommend_env = True

bin_dir = './bin'
bin_dir = os.path.abspath(bin_dir)
if bin_dir not in sys.path: sys.path.append(bin_dir)
import esgfsearch as es

searches_dir = './searches'

# Add command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('name', type=str, help='name of the search (this will be the working directory name, a sub-directory of searches/)')
parser.add_argument('-l', '--local-archive', type=str, help=\
    'location of local data archive (destination for downloaded data). If an absolute path is not given, this will be a subdirectory of the working directory.')
parser.add_argument('-d', '--download-method', type=str, help='method used to download the data (e.g. wget)', default='wget')
# 3 Jun 2019: so far wget is the only download method that's handled.

args = parser.parse_args()

script = 'search.py'
wrk_dir = os.path.join(searches_dir, args.name)

# Create the working dir
print('\nSetting up search_esgf...\n')
if not os.path.exists(wrk_dir):
    os.makedirs(wrk_dir)
    print('Created working directory: ' + wrk_dir)
else:
    print('Working directory {0} already exits.'.format(wrk_dir))

# Create the script that the user will edit to do the searching
script_path = os.path.join(wrk_dir, script)
if not os.path.exists(script_path):
    ###########################################################################
    # Set parameters that will go into the script.
    
    # Location for downloaded files
    if args.local_archive is None:
        # If a destination path for downloaded data isn't given by the user,
        # set the default path to use.
        local_archive = es.local_archive_location()
    else:
        # User has specified the path to use.
        local_archive = args.local_archive
    if os.path.isabs(local_archive):
        # If data path is given as an absolute path.
        # In this case also make a symlink in the working dir to provide
        # convenient access to the local archive.
        symlink = os.path.join(wrk_dir, 'local_archive')
        os.system('ln -s {0} {1}'.format(local_archive, symlink))
    else:
        # If data path is given as a relative path.
        local_archive = os.path.join(wrk_dir, local_archive)


    print('Downloaded data will be put in: {0}'.format(local_archive))
    
    # Location for search summaries
    summary_dir = os.path.join(wrk_dir, 'summary')
    print('Summaries of searches will be put in: {0}'.format(summary_dir))

    ###########################################################################
    # Create the script based on the template in the bin directory (bin_dir)
    # and substitute the parameters into it.
    script_template = os.path.join(bin_dir, script)
    cmd = 'cp {0} {1}'.format(script_template, script_path)
    os.system(cmd)
    print('Created search script: {0}'.format(script_path))
    
    # patt = the regex pattern used to find parameter assignment statements.
    patt = ''';| (?=(?:[^`'"]|'[^']*'|"[^"]*"|`[^`]*`)*$)'''

    # params = list of parameters to substitute
    # Note, the parameter assignments should be on their own lines in the
    # template script file, e.g.
    #   local_archive = './data'
    params = ['local_archive', 'summary_dir', 'bin_dir']
    params.append('latest_git_commit')
    ll = []
    with open(script_path, 'r') as f:
        for line in f:
            line = line.strip('\n')
            l = re.split(patt, line)
            p = l[0]
            if p in params and len(l) == 3:
                if p in ['latest_git_commit']:
                    cmd = 'git rev-parse HEAD'
                    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
                    stdout, stderr = proc.communicate()
                    v = stdout.strip()
                else:
                    exec('v = ' + p)
                if p in ['local_archive', 'summary_dir']:
                    if os.path.commonprefix([wrk_dir, v]).startswith(wrk_dir):
                        # If these are subdirs of the working dir, give them
                        # as relative paths (this just simplifies their
                        # appearance in the script). 
                        v = os.path.join('.', os.path.relpath(v, wrk_dir))
                elif p in ['bin_dir']:
                    # More useful to define bin_dir as a relative path, since then search.py
                    # scripts can be easily copied across different machines.
                    v = os.path.relpath(v, wrk_dir)
                p_types = (str,)
                if sys.version_info.major == 2: p_types += (unicode,)
                if isinstance(v, p_types):
                    l[2] = '\'{0}\''.format(v)
                else:
                    l[2] = '{0}'.format(v)
                line = ' '.join(l)
            ll += [line]
    with open(script_path, 'w') as f:
        f.write('\n'.join(ll) + '\n')
    os.system('chmod 744 ' + script_path)    
else:
    print('Search script {0} already exits.'.format(script_path))

print('To begin searching,')
print('  cd {0}'.format(wrk_dir))
print('and edit {0} as needed.'.format(script))

if recommend_env:
    # Recommend conda env to user.
    env = '/space/hall5/sitestore/eccc/crd/ccrn/users/rja001/miniconda3/envs/rja_search_esgf_v1'
    # Env was creating by the command:
    #   conda create -n rja_search_esgf_v1 python=3 numpy ipython openpyxl
    # and env yaml file was created by (after activating the env):
    #   conda env export --no-builds > env.yml
    print('\nNote, on U2 (ECCC science network) search_esgf will run using this env:')
    print('  ' + env)
    print('If desired, use the env.yml file to create your own conda env for search_esgf.')

print('')